---


---

<h1 id="new-staffstudents-on-boarding">New staff/students on-boarding</h1>
<h2 id="software-tools">Software tools</h2>
<ul>
<li>Notetaking, labbook: Joplin</li>
<li>Code editor, jupyter notebook, git integration: Atom (install the Hydrogen package to allow it to run jupyter kernels) - ask for install from Warren</li>
<li>Remote code editing: vim (painful but useful)</li>
<li>Keeping organized: electronic calendar. To-do list (trello.com) but any will do.</li>
<li>Code diffs, git merges: Meld</li>
<li>Latex: TeXstudio</li>
<li>PDf management: Paperpile</li>
<li>Password manager: Bitwarden (plug in for Chrome, has Android app)</li>
</ul>
<h3 id="terminal-setup">Terminal setup</h3>
<p>Generally it is best to spend as little time typing and clicking as it is slow and inefficient. There are several ways to do this.</p>
<ul>
<li>Learn how to edit your bash start up scripts (<a href="https://bash.cyberciti.biz/guide/Startup_scripts">https://bash.cyberciti.biz/guide/Startup_scripts</a>). Especially handy ones are aliases for commonly visited paths or commands
<ul>
<li>E.g. <code>gosrc</code> takes me to my src directory of CLASSIC code via <code>cd</code> whereas <code>pusrc</code> uses <code>pushd</code> to get to the src directory and you can then use <code>popd</code> to return to where you were before.</li>
</ul>
</li>
<li>Learn keyboard shortcuts for programs like Atom.</li>
</ul>
<h3 id="anaconda-conda-environments">Anaconda (conda environments)</h3>
<p>Conda is a powerful package manager and environment manager that you use with in a terminal window. This is needed to help manage the environment that your python scripts will run in. If not already present:</p>
<ul>
<li>Download the python 3.+ version from <a href="http://anaconda.org">anaconda.org</a></li>
<li>Install by running the script</li>
<li>Information on how to use: <a href="https://conda.io/projects/conda/en/latest/user-guide/getting-started.html">https://conda.io/projects/conda/en/latest/user-guide/getting-started.html</a></li>
<li>Learn how it works and use it! It will save you much frustration down the road.</li>
</ul>
<h1 id="cccma-specific-onboarding-recources">CCCma-specific onboarding recources</h1>
<p>Welcome to the CCCma! Prior to getting started on your project, it is a good<br>
idea to spend some time learning the ECCC supercomputing system and additional<br>
nomenclature you will undoubtedly hear over the next few months.</p>
<h2 id="the-science-network">The Science Network</h2>
<p>During your term you will be working almost solely on the remote ECCC supercomputing<br>
system, both in development and operations - our major models are ran on these<br>
systems and thus the resulting data is typically stored on the systems.</p>
<p>Some details will be discussed below, but the SSC supported documentation can be<br>
at the following two locations:</p>
<ul>
<li><a href="https://portal.science.gc.ca/confluence/">Science Network Documentation</a></li>
<li><a href="https://gitlab.science.gc.ca/hpc_migrations/hpcr_upgrade_1">u1 upgrade gitlab page</a></li>
</ul>
<p>It should be noted that the above links are <em>often</em> not up to date, and thus if<br>
you notice something doesn’t seem right, don’t hesistate to ask your supervisor for assistance.<br>
Additionally, user environments are currently not homogeneous across users, so<br>
if you find that something in this page, or the above links, isn’t working<br>
for you, again, don’t hesistate to ask your supervisor for assistance - it often comes down to<br>
an environment problem.</p>
<h3 id="science-network-machines-and-logging-in">Science Network Machines and Logging In</h3>
<p>When discussing the science network machines, “hall” refers to a specific pairing of two types of machines - “backends” (BE)<br>
and “frontends” (FE), where the BE machines are where we run the heavy<br>
computations, and the FE are where we typically post process the output<br>
data. With this in mind, the machines are grouped as follows:</p>
<h4 id="upgrade-1">Upgrade 1</h4>
<p><em>Note</em>: the <code>u0</code> BE machines are XC50 Cray Machines</p>
<h5 id="hall-3">Hall 3</h5>
<ul>
<li>(FE) <code>ppp3</code></li>
<li>(BE) <code>banting</code></li>
</ul>
<h5 id="hall-4">Hall 4</h5>
<ul>
<li>(FE) <code>ppp4</code></li>
<li>(BE) <code>daley</code></li>
</ul>
<h4 id="gpsc-vis">Gpsc-vis</h4>
<p><strong>will be added</strong></p>
<h4 id="logging-in">Logging In</h4>
<p>There are two main ways that CCCma users log into the above systems, via<br>
<code>tlclient</code> or by <code>ssh</code>ing into the “Landing Pad”</p>
<h5 id="ssh-through-the-landing-pad">ssh Through the Landing Pad</h5>
<p>Unless your <code>ssh</code> has been specifically configured, you will not be able to<br>
directly log into the above machines, and will typically need to go in through<br>
the “Landing Pad”. To do so, all you need to do is execute</p>
<pre class=" language-bash"><code class="prism  language-bash"><span class="token function">ssh</span> abc123@sci-eccc-in.science.gc.ca
</code></pre>
<p>where <code>abc123</code> is replaced with your science network account (note, add the<br>
<code>-Y</code> flag if you wish to enable X11 forwarding, allowing you to use graphical<br>
displays on the remote machine).</p>
<p>After executing this command, you should now be able to ssh to whatever of the<br>
above machines you like!</p>
<p>Note that depending on how comfortable you feel with <code>ssh</code> and <code>alias</code> commands<br>
there are a few ways to “hide” or skip the landing pad - this is not required, but<br>
users are free to explore this.</p>
<p>Additionally, it should be stated, that beyond mild file modifications (i.e.<br>
working in an editor) the landing pad should <strong>NOT</strong> be used for any computation.</p>
<h5 id="thinlinc-client">Thinlinc client</h5>
<p>The Thinlinc client, or <code>tlclient</code>, is program that users can use to log into a KDE desktop environment on a<br>
<code>gpsc-vis</code> machine, within the science network, which is a general purpose machine<br>
that can be used for analysis but can also be used to <code>ssh</code> to the above mentioned machines.</p>
<p>To launch <code>tlclient</code>, depending on your OS and if you have <code>tlclient</code> installed<br>
on your machine, you’ll need to</p>
<ul>
<li>run <code>tlclient</code> from your terminal if in linux, or</li>
<li>run <code>tlclient</code> from the start menu if in windows</li>
</ul>
<p>Then follow the directions in the “Client Setup and Connection”<br>
section on the following <a href="https://portal.science.gc.ca/confluence/display/SCIDOCS/ThinLinc+-+Remote+Desktop+and+Visualisation">page</a><br>
to login.</p>
<p>Once logged in, you’ll need to setup your konsole profile such that it lauches</p>
<pre class=" language-bash"><code class="prism  language-bash"><span class="token function">ssh</span> -Y localhost
</code></pre>
<p>when opened. After this, you’re good to go on <code>gpsc-vis</code> or <code>ssh</code> to other<br>
machines.</p>
<h3 id="important-locations-on-the-science-network">Important Locations on the Science Network</h3>
<p><strong>Note</strong>: replace <code>abc123</code> with your personal science account</p>
<ul>
<li><code>/home/abc123</code> -&gt; your personal home directory, often stored in <code>$HOME</code>
<ul>
<li>This location is shared within upgrades, i.e. the same home directory is<br>
mounted across all <code>u1</code><br>
machines. For example, you can access to same files at <code>$HOME</code> on <code>banting</code><br>
as <code>daley</code> or <code>ppp3/4</code>.</li>
<li>limited to 10GB per user, so you shouldn’t keep large files here</li>
</ul>
</li>
<li><code>/home/ords/crd/ccrn/abc123</code> -&gt; “home ords”
<ul>
<li>This location follows the same access details at <code>$HOME</code></li>
<li>This is a shared storage location for all CCCma staff with significantly<br>
more resources than your <code>$HOME</code>. You can store your larger directories here<br>
to save space in your <code>$HOME</code> directory, but you should still avoid storing<br>
large data files here.</li>
</ul>
</li>
<li><code>/space/hall[34]/sitestore/eccc/crd/ccrn/users/abc123</code> -&gt; sitestore user space
<ul>
<li>located on FE <code>ppp</code> machines, where <code>hall[34]</code> is replaced as <code>hall3</code>, or <code>hall4</code> depending on what hall you are on.</li>
<li>Often stored as <code>$RUNPATH</code> provided you are using the “cccma environment”<br>
(laid out below) and on a FE machine</li>
<li>a <strong>large</strong> storage location on the FE machines</li>
<li>where most model output is stored after it has been transferred off the BE<br>
machines</li>
</ul>
</li>
<li><code>/space/hall[34]/work/eccc/crd/ccrn/users/abc123</code> -&gt; luster user space
<ul>
<li>located on the BE machines, where <code>hall[34]</code> is replaced as <code>hall3</code>, or <code>hall4</code> depending on what hall you are on.</li>
<li>Often stored as <code>$RUNPATH</code> provided you are using the “cccma environment”<br>
(laid out below) and on a BE machine</li>
<li>large storage space on the BE machines that stores large files used in<br>
major computational jobs</li>
</ul>
</li>
<li><code>/space/hall[34]/sitestore/eccc/crd/ccrn/../ccrn_tmp</code> -&gt; FE scratch work space
<ul>
<li>where <code>hall[34]</code> is replaced as <code>hall3</code>, or <code>hall4</code><br>
depending on what hall you are on.</li>
<li>Often stored as <code>$CCRNTMP</code> provided you are using the “cccma environment”<br>
(laid out below) and on a FE machine</li>
<li>temporary scratch space used in most jobs</li>
<li>directories with the <code>tmp</code> prefix here are routinely cleaned on the cadence<br>
of a day or week.</li>
</ul>
</li>
<li><code>/space/hall[34]/work/eccc/crd/ccrn/../ccrn_tmp</code> -&gt; BE scratch work space
<ul>
<li>where <code>hall[34]</code> is replaced as <code>hall3</code>, or <code>hall4</code><br>
depending on what hall you are on.</li>
<li>Often stored as <code>$CCRNTMP</code> provided you are using the “cccma environment”<br>
(laid out below) and on a BE machine</li>
<li>temporary scratch space used in most jobs</li>
<li>directories with the <code>tmp</code> prefix here are routinely cleaned on the cadence<br>
of a day or week.</li>
</ul>
</li>
<li><code>/home/abc123/public_html</code>
<ul>
<li>if not created, feel free to initialize it yourself</li>
<li>any files here can be accessed by going to
<ul>
<li><a href="https://goc-dx.science.gc.ca/~abc123/">https://goc-dx.science.gc.ca/~abc123/</a> if on <code>u1</code> home</li>
</ul>
</li>
<li>can be used to view image files, including <code>html</code> files on the science<br>
network without copying them off. Additionally, you can provide other staff<br>
with links to these pages in order for them to view your images/pages.</li>
</ul>
</li>
</ul>
<h3 id="setting-up-your-environment-dot-files">Setting Up Your Environment (dot files)</h3>
<p>Once you have access to the science network, it is important that you setup your<br>
environment in order to gain access to some SSC and CCCma specific commands, and<br>
environment variables.</p>
<p>To avoid clogging up your environment for all log ins, it is recommended using<br>
<code>~/.profile</code> to set your environment, instead of <code>.bashrc</code> (<code>.profile</code> is only<br>
sourced for login shells, while <code>.bashrc</code> is source for <strong>any new shell</strong>).</p>
<p>Now, in order to setup a basic environment with access to the CCCma specific<br>
environment variables and binaries, as well as those provided by SSC, add the<br>
following lines to your <code>.profile</code> file:</p>
<pre class=" language-bash"><code class="prism  language-bash"><span class="token comment"># set default permissions</span>
<span class="token function">umask</span> 022

<span class="token comment"># load cccma environment</span>
<span class="token keyword">.</span> /home/scrd101/generic/sc_cccma_setup_profile

<span class="token comment"># setup SSC's ORDENV</span>
<span class="token function">export</span> ORDENV_SITE_PROFILE<span class="token operator">=</span>20190814
<span class="token function">export</span> ORDENV_COMM_PROFILE<span class="token operator">=</span>eccc/20191002
<span class="token function">export</span> ORDENV_GROUP_PROFILE<span class="token operator">=</span>eccc/cmc/1.8
<span class="token keyword">.</span> /fs/ssm/main/env/ordenv-boot-20190814.sh
</code></pre>
<p>With these following lines you should be good to go! Just save it and then<br>
execute</p>
<pre class=" language-bash"><code class="prism  language-bash"><span class="token function">source</span> ~/.profile
</code></pre>
<p>to setup your current your environment. Note that this environment will be<br>
<em>automatically</em> sourced for later log ins, so you don’t need to execute the<br>
<code>source</code> command every time.</p>
<p>In addition to the above commands, users can also add things to their <code>.profile</code><br>
to fit their workflow, i.e. <code>alias</code> commands, or extra environment variables.</p>
<p>It should also be noted that as part of the <code>ORDENV</code> setup, SSC has created a<br>
method for sourcing different environments, in addition to <code>.profile</code>,<br>
depending on what host you log onto, or how you are running (i.e. batch mode<br>
or interactive). If you wish to look into this, you are encouraged to see<br>
the “User Profiles” section <a href="https://portal.science.gc.ca/confluence/display/SCIDOCS/ordenv+4+User+Guide">here</a>.<br>
This complexity of your environments can quickly blow up when you use this, so<br>
users are encouraged to keep this in mind - regardless, the most important feature<br>
of this is allowing for different environment configurations when<br>
working in batch mode or interactively. In summary, depending on which mode you’re in,<br>
the following files will come into play (provided they and the necessary directories are created<br>
in your home directory):</p>
<ul>
<li><code>~/.profile.d/default/post</code>
<ul>
<li>sourced for <strong>any</strong> login</li>
</ul>
</li>
<li><code>~/.profile.d/batch/post</code>
<ul>
<li>sourced only for non-tty/terminal sessions - i.e. batch mode</li>
</ul>
</li>
<li><code>~/.profile.d/interactive/post</code>
<ul>
<li>sourced only for tty/terminal sessions - i.e. interactive mode</li>
</ul>
</li>
</ul>
<p>an example interactive file can be found here:</p>
<pre><code>~rcs001/.profile.d/interactive/post
</code></pre>
<p>(<strong>NOTE:</strong> do not copy the commands blindly as some of them<br>
are very likely not relevant to all users)</p>
<h3 id="submitting-your-first-batch-job">Submitting Your First Batch Job</h3>
<p>As alluded to above, a lot of code is typically ran in “batch” mode, meaning that<br>
a job scheduler like <a href="https://slurm.schedmd.com/documentation.html">SLURM</a> or<br>
<a href="https://www.pbspro.org/">PBS Pro</a> takes in “job”, and puts it through a queue<br>
containing other users’ jobs, picking which job runs according to what resources<br>
are currently available on the supercomputer - this is done for two reasons:</p>
<ol>
<li>avoid clogging up the head nodes of the given machine - if a user is running<br>
heavy file manipulation or computational kernel on the headnode, it will<br>
reduce the performance seen by other users currently logged on the system. This<br>
slow down in performance is seen because the headnodes have very limited computational<br>
resources, and by running these type of codes, users end up “hogging” those limited<br>
resources. <strong>Do not perform heavy file manipulations or computations on the head nodes</strong></li>
<li>achieve optimal resource usage on the HPC system in a fair way - the scheduler<br>
determines which job should run by looking at what resources are available, and<br>
how much the given user has recently used. The following things are generally considered:
<ul>
<li>how large is the resource footprint? (many nodes? large amounts of memory? how much time?)</li>
<li>has this user been using a lot of resources lately?</li>
</ul>
</li>
</ol>
<p>With the general concept stated, it should be noted that the specific scheduler used<br>
on our system is PBS Pro, specifically version<br>
19.2.1 on the <code>u1</code> machines.</p>
<p>Now, to setup your first batch job, the key things you need to know is</p>
<ol>
<li>how to specify resources</li>
<li>how to submit the job</li>
</ol>
<p>Fortunately, for item 1, there is a cornucopia of PBS documentation online, as<br>
many HPC centres around the world use the same queuing system, and therefore google<br>
is your friend here. An example will be laid out below, but in addition, the following<br>
links provide some useful resources:</p>
<ul>
<li><a href="https://www.pbsworks.com/pdfs/PBSUserGuide18.2.pdf">PBS 18.2 user guide</a></li>
<li><a href="http://altairgridworks.de/pdfs/PBSUserGuide19.2.3.pdf">PBS 19.2.3 user guide</a> (there may be some changes from 19.2.1)</li>
<li><a href="https://www2.cisl.ucar.edu/resources/computational-systems/cheyenne/running-jobs/submitting-jobs-pbs">NCAR PBS guide</a><br>
and <a href="https://www2.cisl.ucar.edu/resources/computational-systems/cheyenne/running-jobs/submitting-jobs-pbs">example jobscripts</a></li>
<li>many more online…</li>
</ul>
<p>Unfortunately, for item 2, only the BE machines have direct access to the PBS commands, and thus if you<br>
are trying to run things on the <code>ppp</code> machines (which unless you’re performing heavy<br>
computations, you will likely be doing), you must use the queuing commands provided<br>
by SSC as part of their <code>jobctl</code> package. This package is loaded through the <code>ORDENV</code><br>
environment discussed above and is provided to allow for remote submission and<br>
job monitoring across hosts; its documentation can be found<br>
<a href="https://portal.science.gc.ca/confluence/display/SCIDOCS/jobctl1+1.13+User+Guide">here</a>.<br>
Note that this documentation is not extremely comprehensive and it is not entirely clear<br>
as to which version of <code>jobctl</code> you get through the <code>ORDENV</code> setup (<strong>do not</strong><br>
<strong>hesitate to ask your supervisor if you run into any problems</strong>). Fortunately, the functionality<br>
of the necessary <code>jobctl</code> commands are quite simple and can generally be thought of as<br>
analagous to their PBS counterparts (which they eventually call) - these commands will be<br>
discussed below.</p>
<h4 id="specifying-resources">Specifying Resources</h4>
<p>While it is possible to specify resources at the command line, when submitting a<br>
script to run, due to the mixture of <code>jobctl</code> and PBS commands, it is simplest<br>
to focus on specifying them within the “job header”, which can be the same regardless<br>
of the <code>jobctl</code> commands are used or not.</p>
<p>While this document can’t explain <strong>all</strong> the possible header specifications,<br>
and example should provide a good starting point. For example, if one creates<br>
the following file:</p>
<pre class=" language-bash"><code class="prism  language-bash"><span class="token shebang important">#!/bin/bash</span>
<span class="token comment">#PBS -S /bin/bash</span>
<span class="token comment">#PBS -q development</span>
<span class="token comment">#PBS -N my_testjob</span>
<span class="token comment">#PBS -o my_output</span>
<span class="token comment">#PBS -e my_error</span>
<span class="token comment">#PBS -l walltime=01:00:00</span>
<span class="token comment">#PBS -l select=1:ncpus=1:mem=10gb:res_image=ppp_eccc_all_default_ubuntu-14.04-amd64_latest</span>

<span class="token keyword">echo</span> <span class="token string">"My name is <span class="token variable"><span class="token variable">$(</span><span class="token function">whoami</span><span class="token variable">)</span></span>!"</span>
<span class="token keyword">echo</span> <span class="token string">"I'm running in <span class="token variable"><span class="token variable">$(</span><span class="token function">pwd</span><span class="token variable">)</span></span>!"</span>
<span class="token keyword">echo</span> <span class="token string">"I'm running a job on <span class="token variable">$OMP_NUM_THREADS</span> cores!"</span>
<span class="token function">sleep</span> 10
<span class="token keyword">echo</span> <span class="token string">"I'm done!"</span>
</code></pre>
<p>the queuing system will translate the <code>#PBS</code> as “pbs directives”, which<br>
define how we want the job to run. Here is an brief explanation of how each<br>
of these lines is interpreted</p>
<ul>
<li><code>-S /bin/bash</code> -&gt; this job should be ran as a bash shell script</li>
<li><code>-q development</code> -&gt; submit the job to the development queue</li>
<li><code>-N my_testjob</code> -&gt; name the job <code>my_testjob</code></li>
<li><code>-o my_output</code> -&gt; send standard output to a file named <code>my_output</code>
<ul>
<li>without this, it will send to a <code>my_testjob.o*</code> file</li>
</ul>
</li>
<li><code>-e my_error</code> -&gt; send standard error to a fine name <code>my_error</code>
<ul>
<li>without this, it will send to a <code>my_testjob.e*</code> file</li>
</ul>
</li>
<li><code>-l walltime=01:00:00</code> -&gt; limit job to 1 hour execution time</li>
<li><code>-l select=1:ncpus=1:mem=10gb:res_image=ppp_eccc_all_default_ubuntu-14.04-amd64_latest</code>
<ul>
<li><code>select=1:</code> -&gt; request 1 chunk of resources matching those that come after the <code>:</code></li>
<li><code>ncpus=1</code> -&gt; request 1 cpu</li>
<li><code>mem=10gb</code> -&gt; request 10gb of memory</li>
<li><code>res_image=ppp_eccc_all_default_ubuntu-14.04-amd64_latest</code> -&gt; run within the specified container
<ul>
<li>this specification is specific to the <code>jobctl</code> package and should only be<br>
used for jobs that run on the FE machines - BE jobs with this in the header<br>
will not run.</li>
<li>if you wish to run this on a <code>u1</code> FE machine, you’ll need to change it to<br>
<code>res_image=eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest</code></li>
</ul>
</li>
</ul>
</li>
</ul>
<h4 id="submitting-your-job">Submitting your job</h4>
<p>Lets say the above example file is stored in <code>~/tmp/test_job</code>, then to submit the job<br>
to the <code>ppp3</code> machine, from within <code>~/tmp</code>, simply run</p>
<pre class=" language-bash"><code class="prism  language-bash">jobsub -c ppp3 test_job
</code></pre>
<p>where the <code>-c</code> flag is used to specify what host you wish to run the job on. Provided this<br>
works, within a few moments, you should be able to find the error and output files contained<br>
in your home directory! If you want the error and output files to appear in the directory<br>
where you called <code>jobsub</code>, can add the <code>--cwd</code> flag, i.e.</p>
<pre class=" language-bash"><code class="prism  language-bash">jobsub -c ppp3 --cwd test_job
</code></pre>
<p>which claims to make the job start in the current working directory, however, looking at the<br>
standard output from this job shows that the job still starts in the home directory. Nevertheless<br>
the output/error files are still created in the current working directory.</p>
<p>If you wanted to submit this job to run on a BE machine, as discussed above, you would need<br>
to remove the <code>res_image</code> specification. After that, you would simply need to replace the <code>ppp3</code><br>
arg with the BE hostname. Note that you could also use <code>qsub</code> if currently logged onto a BE machine.<br>
If you do this, you’ll need to remove the <code>-c</code> and <code>--cwd</code> flag, as these are not<br>
PBS options; the pbs commands always use the current host, and will <em>by default</em> write the output/error<br>
files to the current working directory.</p>
<h4 id="monitoring-your-job">Monitoring Your Job</h4>
<p>In order to monitor your jobs and see if they are running or queued, you can do so by<br>
invoking</p>
<pre class=" language-bash"><code class="prism  language-bash">jobst -c JOB_HOST -u <span class="token variable"><span class="token variable">$(</span><span class="token function">whoami</span><span class="token variable">)</span></span>
</code></pre>
<p>for any job running on the BE’s or FE’s, where <code>JOB_HOST</code> should be be replaced<br>
with the machine name for where your job is running (`$(whoami) will automatically<br>
expand out to your username).</p>
<p>If you are on the BE machines and wanting to monitor a job running there, you can use</p>
<pre class=" language-bash"><code class="prism  language-bash">qstat -u <span class="token variable"><span class="token variable">$(</span><span class="token function">whoami</span><span class="token variable">)</span></span>
</code></pre>
<h4 id="halting-a-job">Halting a Job</h4>
<p>There are many instances where a user would like to halt their job, instead of allowing<br>
it to sit in the queue or keep producing incorrect answers. Fortunately, this can<br>
easily be done by first finding the jobid for the job you would like to cancel, by<br>
invoking <code>jobst</code> or <code>qstat</code> (described above), and then running</p>
<pre class=" language-bash"><code class="prism  language-bash">jobdel -c JOB_HOST JOBID
</code></pre>
<p>where JOB_HOST should be replaced by the host running the job, and JOBID is the<br>
jobid determined from <code>jobst</code> or <code>qstat</code>.</p>
<p>Again, if you are on a BE machine and would like to cancel a job running there<br>
you can run</p>
<pre class=" language-bash"><code class="prism  language-bash">qdel JOBID
</code></pre>
<h2 id="additional-resources">Additional Resources</h2>
<ul>
<li><a href="https://gitlab.science.gc.ca/">science network gitlab</a></li>
<li><a href="https://gitlab.science.gc.ca/CanESM/Python_Envs">getting access to the CCCma specific conda install on the science network</a></li>
<li><a href="https://gitlab.science.gc.ca/CanESM/technical_development_cycle">technical development cycle</a></li>
</ul>
<blockquote>
<p>Written with <a href="https://stackedit.io/">StackEdit</a>.</p>
</blockquote>

