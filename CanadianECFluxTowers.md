
# Already in CLASSIC framework

- CA-Qfo http://sites.fluxdata.org/CA-Qfo/
  - Quebec - Eastern Boreal, Mature Black Spruce
  - Hank A. Margolis (Hank.Margolis@sbf.ulaval.ca)		
  - ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	962.32	-0.36	Canada	49.6925	-74.3420600	382	2003
- CA-WP1
  - Alberta - Western Peatland - LaBiche River,Black Spruce/Larch Fen	
  - Lawrence B. Flanagan (larry.flanagan@uleth.ca)	
  - WET	Dfc	Subarctic: severe winter, no dry season, cool summer	461.07	1.1	Canada	54.9538400	-112.4669800	540	2003
- CA-Mer	
  - Ontario - Eastern Peatland, Mer Bleue	
  - Elyn Humphreys (elyn.humphreys@carleton.ca)		
  - WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfb	Warm Summer Continental: significant precipitation in all seasons 	943	6	Canada	45.4094	-75.5186	70	  
- CA-SCB	
  - Scotty Creek Bog	
  - Oliver Sonnentag, William L Quinton (wquinton@wlu.ca)		
  - WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfc	Subarctic: severe winter, no dry season, cool summer	388	-2.8	Canada	61.3089	-121.2984	280	2014

# Being integrated by someone else

- CA-Obs http://sites.fluxdata.org/CA-Obs/ (**Bo Qu**)
  - Saskatchewan - Western Boreal, Mature Black Spruce	
  - T. Andrew Black (andrew.black@ubc.ca)		
  - ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	405.6	0.79	Canada	53.9871700	-105.1177900	628.94	1997, 1998
- CA-Man http://sites.fluxdata.org/CA-Man/ (**Bo Qu**)
  - Manitoba - Northern Old Black Spruce (former BOREAS Northern Study Area)	
  - Brian Amiro (Brian_Amiro@umanitoba.ca)		
  - ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	520	-3.2	Canada	55.8796200	-98.4808100	259	1994, 1995, 1996
- CA-Ojp (**Bo Qu**)
  - Saskatchewan - Western Boreal, Mature Jack Pine	
  - Andrew T. Black (andrew.black@ubc.ca)	
  - ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	430.5	0.12	Canada	53.9163400	-104.6920300	579	1996, 1997, 1998
 - CA-DL1 (**Gesa**)	
   - Daring Lake - Mixed Tundra	
   - Elyn Humphreys (elynhumphreys@cunet.carleton.ca), Peter Lafleur (plafleur@trentu.ca)		
   - OSH Open Shrublands: Lands with woody vegetation less than 2 meters tall and with shrub canopy cover between 10-60%. The shrub foliage can be either evergreen or deciduous.	Dfc	Subarctic: severe winter, no dry season, cool summer	250	-9	Canada	64.8688553	-111.5747927	425	
- CA-ARB (**Jason Beaver**)	
  - Attawapiskat River Bog	
  - Elyn Humphreys (elynhumphreys@cunet.carleton.ca)		
  - WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfb	Warm Summer Continental: significant precipitation in all seasons 	700	-1.3	Canada	52.694999	-83.945226	90	2011
- CA-ARF (**Jason Beaver**)	
  - Attawapiskat River Fen	
  - Elyn Humphreys (elynhumphreys@cunet.carleton.ca)		
  - WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfb	Warm Summer Continental: significant precipitation in all seasons 	700	-1.3	Canada	52.70078	-83.955045	88	2011
	300	1999
- CA-HPC (**Bo Qu**)
  - Havikpak Creek	
  - Oliver Sonnentag (oliver.sonnentag@umontreal.ca)		
  - ENF	Evergreen Needleleaf Forests: Lands dominated by woody vegetation with a percent cover >60% and height exceeding 2 meters. Almost all trees remain green all year. Canopy is never without green foliage.	Dfc	Subarctic: severe winter, no dry season, cool summer	240.6	-8.2	Canada	68.32029	-133.51878	80	
  
# FLUXNET2015 or have previously run

- CA-Gro http://sites.fluxdata.org/CA-Gro/
  - Ontario - Groundhog River, Boreal Mixedwood Forest	Harry McCaughey (mccaughe@queensu.ca)		MF	Dfb	Warm Summer Continental: significant precipitation in all seasons 	831	1.3	Canada	48.2167	-82.1556	340	2003
- CA-Oas http://sites.fluxdata.org/CA-Oas/
  - Saskatchewan - Western Boreal, Mature Aspen	T. Andrew Black (andrew.black@ubc.ca)		DBF	 Dfc	Subarctic: severe winter, no dry season, cool summer	428.53	0.34	Canada	53.6288900	-106.1977900	530	1996, 1997, 1998

- CA-SF1 http://sites.fluxdata.org/CA-SF1/
  - Saskatchewan - Western Boreal, forest burned in 1977	Brian Amiro (Brian_Amiro@umanitoba.ca)		ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	470	0.4	Canada	54.4850300	-105.8175700	536	2003
- CA-SF2 http://sites.fluxdata.org/CA-SF2/
  - Saskatchewan - Western Boreal, forest burned in 1989	Brian Amiro (Brian_Amiro@umanitoba.ca)		ENF		Dfc	Subarctic: severe winter, no dry season, cool summer	470	0.4	Canada	54.2539200	-105.8775000	520	2001
- CA-SF3 http://sites.fluxdata.org/CA-SF3/
  - Saskatchewan - Western Boreal, forest burned in 1998	Brian Amiro (Brian_Amiro@umanitoba.ca) **OSH** Dfc	Subarctic: severe winter, no dry season, cool summer	470	0.4	Canada	54.0915600	-106.0052600	540	2001
- CA-NS1 http://sites.fluxdata.org/CA-NS1/
  - UCI-1850 burn site	Mike Goulden (mgoulden@uci.edu)		ENF		Dfc	Subarctic: severe winter, no dry season, cool summer	500.29	-2.89	Canada	55.8791700	-98.4838900	260	2001
- CA-NS2 http://sites.fluxdata.org/CA-NS2/
  - UCI-1930 burn site	Mike Goulden (mgoulden@uci.edu)		ENF		Dfc	Subarctic: severe winter, no dry season, cool summer	499.82	-2.88	Canada	55.9058300	-98.5247200	260	2001
- CA-NS3 http://sites.fluxdata.org/CA-NS3/
  - UCI-1964 burn site	Mike Goulden (mgoulden@uci.edu)		ENF		Dfc	Subarctic: severe winter, no dry season, cool summer	502.22	-2.87	Canada	55.9116700	-98.3822200	260	2001
- CA-NS4 http://sites.fluxdata.org/CA-NS4/
  - UCI-1964 burn site wet	Mike Goulden (mgoulden@uci.edu)		ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	502.22	-2.87	Canada	55.914370	-98.380645	260	2001
- CA-NS5 http://sites.fluxdata.org/CA-NS5/
  - UCI-1981 burn site	Mike Goulden (mgoulden@uci.edu)		ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	500.34	-2.86	Canada	55.8630600	-98.4850	260	2001
- CA-NS6 http://sites.fluxdata.org/CA-NS6/
  - UCI-1989 burn site	Mike Goulden (mgoulden@uci.edu)		**OSH**	Dfc	Subarctic: severe winter, no dry season, cool summer	495.37	-3.08	Canada	55.9166700	-98.9644400	244	2001
- CA-NS7 http://sites.fluxdata.org/CA-NS7/
  - UCI-1998 burn site	Mike Goulden (mgoulden@uci.edu)		**OSH**		Dfc	Subarctic: severe winter, no dry season, cool summer	483.27	-3.52	Canada	56.6358300	-99.9483300	297	2001
- CA-TP1 http://sites.fluxdata.org/CA-TP1/
  - Ontario - Turkey Point 2002 Plantation White Pine	M. Altaf Arain (arainm@mcmaster.ca)		ENF		Dfb	Warm Summer Continental: significant precipitation in all seasons 	1036	8	Canada	42.66093611	-80.55951944	265	2002, 2003, 2004, 2005 
- CA-TP2 http://sites.fluxdata.org/CA-TP2/
  - Ontario - Turkey Point 1989 Plantation White Pine	M. Altaf Arain (arainm@mcmaster.ca)		ENF	Dfb	Warm Summer Continental: significant precipitation in all seasons 	1036	8	Canada	42.77441944	-80.458775	212	2002
- CA-TP3 http://sites.fluxdata.org/CA-TP3/
  - Ontario - Turkey Point 1974 Plantation White Pine	M. Altaf Arain (arainm@mcmaster.ca)	ENF		Dfb	Warm Summer Continental: significant precipitation in all seasons 	1036	8	Canada	42.70681111	-80.34831389	184	2002, 2003, 2004, 2005
- CA-TP4 http://sites.fluxdata.org/CA-TP4/
  - Ontario - Turkey Point 1939 Plantation White Pine	M. Altaf Arain (arainm@mcmaster.ca)		ENF	Dfb	Warm Summer Continental: significant precipitation in all seasons 	1036	8	Canada	42.710161	-80.357376	184	2002, 2003, 2004, 2005
- CA-TPD http://sites.fluxdata.org/CA-TPD/
  - Ontario - Turkey Point Mature Deciduous	M. Altaf Arain (arainm@mcmaster.ca)	DBF	Dfb	Warm Summer Continental: significant precipitation in all seasons 	1036	8	Canada	42.635328	-80.557731	260	2012

  
# Possible (many years)

- CA-SMC	
  - Smith Creek	
  - Oliver Sonnentag (oliver.sonnentag@umontreal.ca)		
  ENF	Evergreen Needleleaf Forests: Lands dominated by woody vegetation with a percent cover >60% and height exceeding 2 meters. Almost all trees remain green all year. Canopy is never without green foliage.	Dfc	Subarctic: severe winter, no dry season, cool summer	388	-2.8	Canada	63.1534	-123.25223	150
- CA-TVC
  - Trail Valley Creek	
  - Oliver Sonnentag (oliver.sonnentag@umontreal.ca)		
  - OSH	Open Shrublands: Lands with woody vegetation less than 2 meters tall and with shrub canopy cover between 10-60%. The shrub foliage can be either evergreen or deciduous.	Dfc	Subarctic: severe winter, no dry season, cool summer	240.6	-8.2	Canada	68.74617	-133.50171	85	
- CA-SCC	
  - Scotty Creek Landscape	
  - William L Quinton (wquinton@wlu.ca)		
  - ENF	Evergreen Needleleaf Forests: Lands dominated by woody vegetation with a percent cover >60% and height exceeding 2 meters. Almost all trees remain green all year. Canopy is never without green foliage.	Dfc	Subarctic: severe winter, no dry season, cool summer	387.6	-2.8	Canada	61.3079	-121.2992	285	2013
- CA-Ca1
  - British Columbia - 1949 Douglas-fir stand	T. Andrew Black (andrew.black@ubc.ca)	ENF	Cfb	Marine West Coast: mild with no dry season, warm summer	1369	9.93	Canada	49.8673	-125.3336	300	1996, 1997, 1998
- CA-Cbo
  - Ontario - Mixed Deciduous, Borden Forest Site	Ralf Staebler (Ralf.Staebler@canada.ca)	DBF Dfb	Warm Summer Continental: significant precipitation in all seasons 	876.34	6.66	Canada	44.3166700	-79.9333	120	1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002
- CA-Mer	
  - Ontario - Eastern Peatland, Mer Bleue	Elyn Humphreys (elyn.humphreys@carleton.ca)	*WET*	Dfb	Warm Summer Continental: significant precipitation in all seasons 	943	6	Canada	45.4094	-75.5186	70	
- CA-DL2	
  - Daring Lake Fen	
  - Peter Lafleur (plafleur@trentu.ca)		
  - WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfc	Subarctic: severe winter, no dry season, cool summer	250	-9	Canada	64.8648266	-111.5677015	416	
  
# Likely not useful

- CA-Ca2
  - British Columbia - Clearcut Douglas-fir stand (harvested winter 1999/2000)	T. Andrew Black (andrew.black@ubc.ca)		ENF	Cfb	Marine West Coast: mild with no dry season, warm summer	1474	9.86	Canada	49.8705	-125.2909	300	1999
- CA-Ca3
  - British Columbia - Pole sapling Douglas-fir stand	T. Andrew Black (andrew.black@ubc.ca)		ENF	Cfb	Marine West Coast: mild with no dry season, warm summer	1676	9.94	Canada	49.5346	-124.9004		2000
- CA-Cha
  - New Brunswick - Charlie Lake site 01 (immature balsam fir forest to be thinned in year 3)	Charles Bourque (cbourque@unb.ca)	MF	Dfb	Warm Summer Continental: significant precipitation in all seasons 	971.68	3.46	Canada	45.8847	-67.3569	341	2004
- CA-Fen	
  - Saskatchewan - Western Boreal, wetland	T. Andrew Black (andrew.black@ubc.ca)	*WET*	Dfc	Subarctic: severe winter, no dry season, cool summer	432.84	0.18	Canada	53.8020600	-104.6179800		
- CA-Let	
  - Alberta - Mixed Grass Prairie	Lawrence B. Flanagan (larry.flanagan@uleth.ca)	GRA 	Dfb	Warm Summer Continental: significant precipitation in all seasons 	398.4	5.36	Canada	49.7092800	-112.9401700	960	1998
  - **Same site in Alan's paper? But says only 1 year.**
- CA-Na1	
  - New Brunswick - 1967 Balsam Fir - Nashwaak Lake Site 01 (Mature balsam fir forest)	Charles P.-A. Bourque (cbourque@unb.ca)	ENF	Dfb	Warm Summer Continental: significant precipitation in all seasons 	1102.7	7.09	Canada	46.4722	-67.1000	341	2003
- CA-Qc2	
  - Quebec - 1975 Harvested Black Spruce (HBS75)	Hank Margolis (Hank.Margolis@sbf.ulaval.ca)	MF		Canada	49.7598	-74.5711		2007
- CA-Qcu	
  - Quebec - Eastern Boreal, Black Spruce/Jack Pine Cutover	Hank A. Margolis (Hank.Margolis@sbf.ulaval.ca)	ENF Dfc	Subarctic: severe winter, no dry season, cool summer	949.78	0.13	Canada	49.2671200	-74.0365	392.3	2001
- CA-SJ1
  - Saskatchewan - Western Boreal, Jack Pine forest harvested in 1994	Alan Barr (alan.barr@ec.gc.ca)		ENF		Dfc	Subarctic: severe winter, no dry season, cool summer	430.23	0.13	Canada	53.9080	-104.6560	580	2000
- CA-SJ2
  - Saskatchewan - Western Boreal, Jack Pine forest harvested in 2002	Andrew T. Black (andrew.black@ubc.ca)		ENF	Dfc	Subarctic: severe winter, no dry season, cool summer	430.03	0.11	Canada	53.9450	-104.6490	580	2002
- CA-SJ3	
  - Saskatchewan - Western Boreal, Jack Pine forest harvested in 1975 (BOREAS Young Jack Pine)	Alan Barr (alan.barr@ec.gc.ca)		ENF		Dfc	Subarctic: severe winter, no dry season, cool summer	433.33	0.13	Canada	53.8758100	-104.6452900		2002
- CA-WP2	
  - Alberta - Western Peatland - Poor Fen (Sphagnum moss)	Lawrence B. Flanagan (larry.flanagan@uleth.ca)	*WET*	Dfc	Subarctic: severe winter, no dry season, cool summer			Canada	55.5375	-112.3343000		2004
- CA-WP3	
  - Alberta - Western Peatland - Rich Fen  (Carex)	Lawrence B. Flanagan (larry.flanagan@uleth.ca)	*WET*	Dfc	Subarctic: severe winter, no dry season, cool summer			Canada	54.4700	-113.3200		2004
  - 
  
  
  ----------------------------------
  
- CA-CF1	
  - Churchill Fen Site 1	Tim Papakyriakou (Tim.papakyriakou@umanitoba.ca)		WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfc	Subarctic: severe winter, no dry season, cool summer	452	-6.5	Canada	58.6658	-93.8300	16.5
- CA-CF2	
  - Churchill Fen Site 2	Mario Tenuta (Mario.Tenuta@umanitoba.ca)		WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfc	Subarctic: severe winter, no dry season, cool summer	452	-6.5	Canada	58.6658	-93.8300	16.5	
- CA-DBB	
  - Delta Burns Bog	Sara Knox (sara.knox@ubc.ca)		WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Csb	Mediterranean: mild with dry, warm summer	1128	10	Canada	49.1293	-122.9849	4	2014
- CA-ER1	
  - Elora Research Station	Claudia Wagner-Riddle (cwagnerr@uoguelph.ca)		CRO	Croplands: Lands covered with temporary crops followed by harvest and a bare soil period (e.g., single and multiple cropping systems). Note that perennial woody crops will be classified as the appropriate forest or shrub land cover type.	Dfb	Warm Summer Continental: significant precipitation in all seasons 	946	6.7	Canada	43.640458	-80.412303	370	2015
- CA-KLP	
  - Kinoje Lake Peatland	Aaron Todd (aaron.todd@ontario.ca)		WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfb	Warm Summer Continental: significant precipitation in all seasons 	680	-1.1	Canada	51.59024	-81.76840	71	
- CA-LuM	
  - Luther Marsh Wildlife Management Area	Elisa Fleischer (elisa.fleischer@uni-muenster.de)		WET	Permanent Wetlands: Lands with a permanent mixture of water and herbaceous or woody vegetation that cover extensive areas. The vegetation can be present in either salt, brackish, or fresh water	Dfb	Warm Summer Continental: significant precipitation in all seasons 	905	6.6	Canada	43.920106	-80.405155	485	
- CA-MA1	
  - Manitoba Agricultural Site 1	Brian Amiro (brian_amiro@umanitoba.ca)		CRO	Croplands: Lands covered with temporary crops followed by harvest and a bare soil period (e.g., single and multiple cropping systems). Note that perennial woody crops will be classified as the appropriate forest or shrub land cover type.	Dfb	Warm Summer Continental: significant precipitation in all seasons 	514	2.6	Canada	50.164472	-97.876222	261	
- CA-MA2	
  - Manitoba Agricultural Site 2	Brian Amiro (brian_amiro@umanitoba.ca)		GRA	Grasslands: Lands with herbaceous types of cover. Tree and shrub cover is less than 10%. Permanent wetlands lands with a permanent mixture of water and herbaceous or woody vegetation. The vegetation can be present in either salt, brackish, or fresh water.	Dfb	Warm Summer Continental: significant precipitation in all seasons 	514	2.6	Canada	50.170972	-97.876222	261	
- CA-MA3	
  - Manitoba Agricultural Site 3	Brian Amiro (brian_amiro@umanitoba.ca)		GRA	Grasslands: Lands with herbaceous types of cover. Tree and shrub cover is less than 10%. Permanent wetlands lands with a permanent mixture of water and herbaceous or woody vegetation. The vegetation can be present in either salt, brackish, or fresh water.	Dfb	Warm Summer Continental: significant precipitation in all seasons 	514	2.6	Canada	50.177417	-97.868639	261	
- CA-MR3	
  - Mattheis Ranch - E3 South	John Gamon (jgamon@gmail.com)		GRA	Grasslands: Lands with herbaceous types of cover. Tree and shrub cover is less than 10%. Permanent wetlands lands with a permanent mixture of water and herbaceous or woody vegetation. The vegetation can be present in either salt, brackish, or fresh water.	Dwd	Subarctic: severe, very cold and dry winter, cool summer	325		Canada	50.8671	-111.9045	712	2012
- CA-MR5	
  - Mattheis Ranch - E5 North	John Gamon (jgamon@gmail.com)		GRA	Grasslands: Lands with herbaceous types of cover. Tree and shrub cover is less than 10%. Permanent wetlands lands with a permanent mixture of water and herbaceous or woody vegetation. The vegetation can be present in either salt, brackish, or fresh water.	Dwd	Subarctic: severe, very cold and dry winter, cool summer	325		Canada	50.9056	-111.8823	700	2012
- CA-NS8	
  - UCI-2003 burn site	Mike Goulden (mgoulden@uci.edu)		ENF	Evergreen Needleleaf Forests: Lands dominated by woody vegetation with a percent cover >60% and height exceeding 2 meters. Almost all trees remain green all year. Canopy is never without green foliage.	Dfc	Subarctic: severe winter, no dry season, cool summer	506.69	-2.71	Canada	55.8980600	-98.2161100	274	2001
- CA-RPn	
  - Robinsons_Natural_Bog_NL	Jianghua Wu (jwu@grenfell.mun.ca)		OSH	Open Shrublands: Lands with woody vegetation less than 2 meters tall and with shrub canopy cover between 10-60%. The shrub foliage can be either evergreen or deciduous.	Dfb	Warm Summer Continental: significant precipitation in all seasons 	1340.4	5	Canada	48.2604	-58.6632	150	
- CA-RPp
  - Robinsons_Peatland_Pasture_NL	Jianghua Wu (jwu@grenfell.mun.ca)OSH	Open Shrublands: Lands with woody vegetation less than 2 meters tall and with shrub canopy cover between 10-60%. The shrub foliage can be either evergreen or deciduous.	Dfb	Warm Summer Continental: significant precipitation in all seasons 	1340.4	5	Canada	48.2632	-58.6670	162	


> Written with [StackEdit](https://stackedit.io/).


> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTA5NjQwNzE1Nl19
-->