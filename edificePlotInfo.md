# 2D Geographical Plots

 - Below are the colour schemes Vivek used. Search for LinearSegmentedColormap.from_list to specify more color schemes:

1. White_LightBlue_Green_Yellow_Orange_Red_Brown - use for usual plots
 2. Brown_Red_Orange_Yellow_Green_LightBlue_White - inverse of above and used for net long wave radiation with all -ve values
 3.  Mustard_White_DarkBlue - use for usual anomalies plots with white in between
4. Magenta_White_LightBlue_Green_Yellow_Orange_Red_Brown - Used for sensible heat plot with some -ve values

Search his code for these and you can see where they are specified.

These plots are just 2D plots of the variable. Select something like the last N years and take the mean to get a 2D field for plotting.

## Net shortwave

file="rss_annually.nc"
title= "Net downward shortwave radiation" 
colorlevs= "1, 30, 50, 80, 110, 140, 160, 180, 200, 220" >> plot_features.txt # color bar levels 
label "\$W\ m^{-2} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Net longwave

file="rls_annually.nc"
echo "Net downward longwave radiation" >> plot_features.txt # plot_title
echo "-150, -125, -100, -75, -50, -40, -30, -20, -10, 0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$W\ m^{-2} $" >> plot_features.txt # color bar title
echo "Brown_Red_Orange_Yellow_Green_LightBlue_White" >> plot_features.txt # color scheme
plot_2D_geographical_plot_last_N_years $file

## NPP

file="npp_annually.nc"
echo "Net primary productivity" >> plot_features.txt # plot_title
echo "25, 50, 100, 150, 250, 500, 750, 1000, 1250, 1500" >> plot_features.txt # color bar levels (maximum 10)
echo "\$g\ C\ m^{-2}\ year^{-1} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
**Conversion:** Need gC/m2/yr so do kg C/m2/s  x 31536000 = kg C/m2/year x 1000 = g C/m2/year

## Precipitation

file="pr_annually.nc"
echo "Precipitation" >> plot_features.txt # plot_title
echo "10, 100, 300, 500, 700, 1000, 1300, 1800, 2500, 3200" >> plot_features.txt # color bar levels (maximum 10)
echo "\$mm\ year^{-1}$" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
**Conversion:** kg/m2.s == mm/s x 31536000 = mm/year

## Net biome productivity

file="nbp_annually.nc"
echo "Net biome production" >> plot_features.txt # plot_title
echo "-90.0, -70.0, -50.0, -30.0, -10.0, 10.0, 30.0, 50.0, 70.0, 90.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$g\ C\ m^{-2}\ year^{-1} $" >> plot_features.txt # color bar title
echo "Mustard_White_DarkBlue" >> plot_features.txt # color scheme
**Conversion:** Kg C/m2.s  x 31536000 = kg C/m2.year x 1000 = g C/m2.year

## Sensible heat flux

file="hfss_annually.nc"
echo "Sensible heat flux" >> plot_features.txt # plot_title
echo "-5.0, -0.5, 0.5, 5.0, 10.0, 20.0, 30.0, 40.0, 50.0, 60.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$ W/m^{-2} $" >> plot_features.txt # color bar title
echo "Magenta_White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Latent  heat flux

file="hfls_annually.nc"
echo "Latent heat flux" >> plot_features.txt # plot_title
echo "0.50, 5.0, 10.0, 20.0, 40.0, 60.0, 80.0, 100.0, 140.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$ W/m^{-2} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Total evapotranspiration

file="evspsbl_annually.nc"
echo "Total evapotranspiration" >> plot_features.txt # plot_title
echo "10, 50, 100, 250, 400, 700, 900, 1100, 1300, 1600" >> plot_features.txt # color bar levels (maximum 10)
echo "\$mm\  year^{-1} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
**Conversion:** Kg/m2.s == mm/s x 31536000 = mm/year
plot_2D_geographical_plot_last_N_years $file 31536000

## GPP

file="gpp_annually.nc"
echo "Gross primary productivity" >> plot_features.txt # plot_title
echo "50, 100, 200, 300, 500, 1000, 1500, 2000, 2500, 3000" >> plot_features.txt # color bar levels (maximum 10)
echo "\$g\ C\ m^{-2}\ year^{-1} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
**Conversion:** Kg C/m2.s  x 31536000 = kg C/m2.year x 1000 = g C/m2.year

## Total land carbon

file="cLand_annually.nc"
echo "Total land carbon" >> plot_features.txt # plot_title
echo "0.5, 1.0, 2.0, 5.0, 10.0, 15.0, 20.0, 25.0, 30.0, 40.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$Kg\ C\ m^{-2} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Albedo

file="albs_annually.nc"
echo "Albedo" >> plot_features.txt # plot_title
echo "0.01, 0.03, 0.06, 0.15, 0.20, 0.25, 0.35, 0.45, 0.60, 0.75" >> plot_features.txt # color bar levels (maximum 10)
echo "Fraction" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Vegetation biomass

file="cVeg_annually.nc"
echo "Vegetation biomass" >> plot_features.txt # plot_title
echo "0.10, 0.25, 0.5, 1.0, 2.0, 6.0, 10.0, 14.0, 18.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$Kg\ C\ m^{-2} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Fire CO2 emissions

file="fFire_annually.nc"
echo "\$Fire\ CO_{2}\ emissions\$ " >> plot_features.txt # plot_title
echo "1, 5, 10, 50, 100, 200, 300, 500, 1000" >> plot_features.txt # color bar levels (maximum 10)
echo "\$g\ CO_{2}\ m^{-2}\ year^{-1}  $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Soil carbon

file="cSoil_annually.nc"
echo "Soil carbon mass" >> plot_features.txt # plot_title
echo "0.50, 1.0, 2.0, 4.0, 8.0, 12.0, 20.0, 30.0, 50.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$Kg\ C\ m^{-2} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme

## Annual maximum active layer depth

file="actlyrmax_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Annual maximum active layer depth" >> plot_features.txt # plot_title
echo "0.1, 1, 10, 15, 20, 25, 30, 40, 50, 60" >> plot_features.txt # color bar levels (maximum 10)
echo "\$\ m $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
plot_2D_geographical_plot_last_N_years $file

## Annual Litter fall

file="fVegLitter_annually.nc"
echo "Annual litter fall" >> plot_features.txt # plot_title
echo "1, 50, 100, 200, 300, 500, 700, 900, 1100, 1300" >> plot_features.txt # color bar levels (maximum 10)
echo "\$gC\ m^{-2}\ year^{-1} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme


---------------------------

# Annual time series

## Deforested biomass total

file="fDeforestTotal_annually.nc"
echo "Total deforested biomass" >> plot_features.txt # plot_title
echo "Total deforested biomass (Pg C/yr)" >> plot_features.txt # y_label,
**Conversion**: Units are kgC/m2 (this is a pool) so we multiply by the grid area to get kgC/gridcell then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## Total land carbon

file="cLand_annually.nc"
echo "Total land carbon" >> plot_features.txt # plot_title
echo "Land carbon mass (Pg)" >> plot_features.txt # y_label
**Conversion**: Units are kgC/m2 (this is a pool) so we multiply by the grid area to get kgC/gridcell then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## GPP

file="gpp_annually.nc"
echo "Gross primary productivity" >> plot_features.txt # plot_title
echo "Gross primary productivity (Pg C/yr)" >> plot_features.txt # y_label
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## NPP 

file="npp_annually.nc"
echo "Net primary productivity" >> plot_features.txt # plot_title
echo "Net primary productivity (Pg C/yr)" >> plot_features.txt # y_label,
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## NBP

file="nbp_annually.nc"
echo "Net biome productivity" >> plot_features.txt # plot_title
echo "Net biome productivity (Pg C/yr)" >> plot_features.txt # y_label,
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## RA 

file="ra_annually.nc"
echo "Autotrophic respiration" >> plot_features.txt # plot_title
echo "Autotrophic respiration(Pg C/yr)" >> plot_features.txt # y_label, 
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## RH

file="rh_annually.nc"
echo "Heterotrophic respiration" >> plot_features.txt # plot_title
echo "Heterotrophic respiration(Pg C/yr)" >> plot_features.txt # y_label,
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## Litter Resp

file="rhLitter_annually.nc"
echo "Litter respiration" >> plot_features.txt # plot_title
echo "Litter respiration(Pg C/yr)" >> plot_features.txt # y_label,
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## Soil C resp

file="rhSoil_annually.nc"
echo "Soil carbon respiration" >> plot_features.txt # plot_title
echo "Soil carbon respiration(Pg C/yr)" >> plot_features.txt # y_label, 
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## FIRE CO2 FLUX

file="fFire_annually.nc"
echo "Global fire emissions" >> plot_features.txt # plot_title
echo "\"'Fire CO'[2]*' emissions (Pg C/yr)'\"" >> plot_features.txt # y_label, **Make the 2 a subscript**
**Conversion**: Units are **kgCO2/m2/s** (this is a flux) so we multiply 12.01/44.01 to convert from CO2 to C then by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## FIRE CH4 FLUX

file="fFireCH4_annually.nc"
echo "Global fire methane emissions" >> plot_features.txt # plot_title
echo "\"'Fire CH'[4]*' emissions (Tg CH'[4]*'/yr)'\"" >> plot_features.txt # y_label, **the 4 is a subscript**
**Conversion**: Units are **kgCH4/m2/s** (this is a flux) so by the grid area to get kgCH4/gridcell/s multiply by the number of seconds in a year to get kgCH4/gridcell/year then divide by 1E9 to get TgCH4. Sum across all cells to get an annual time series.

## SOIL CH4 FLUX

file="soilCH4cons_annually.nc"
echo "Global soil methane uptake" >> plot_features.txt # plot_title
echo "\"'Soil CH'[4]*' uptake (Tg CH'[4]*'/yr)'\"" >> plot_features.txt # y_label, 
**Conversion**: Units are **kgCH4/m2/s** (this is a flux) so by the grid area to get kgCH4/gridcell/s multiply by the number of seconds in a year to get kgCH4/gridcell/year then divide by 1E9 to get TgCH4. Sum across all cells to get an annual time series.

## WETLAND CH4 EMISSIONS FROM DYNAMIC WETLANDS

file="wetlandCH4dyn_annually.nc"
echo "Global wetland methane emissions" >> plot_features.txt # plot_title
echo "\"'Wetland CH'[4]*' emissions (Tg CH'[4]*'/yr)'\"" >> plot_features.txt # y_label,  **Conversion**: Units are **kgCH4/m2/s** (this is a flux) so by the grid area to get kgCH4/gridcell/s multiply by the number of seconds in a year to get kgCH4/gridcell/year then divide by 1E9 to get TgCH4. Sum across all cells to get an annual time series.

## CVEG 

file="cVeg_annually.nc"
echo "Global vegetation biomass" >> plot_features.txt # plot_title
echo "Vegetation biomass (Pg)" >> plot_features.txt # y_label,
**Conversion**: Units are **kgC/m2** (this is a pool) so by the grid area to get kgC/gridcell then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## LAI

file="lai_annually.nc"
echo "Globally averaged annual max. LAI over land (excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Leaf area index (m'^2*'/m'^2*')'\"" >> plot_features.txt # y_label, 
**Conversion**: Units are **m2/m2** so sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## C LITTER

file="cLitter_annually.nc"
echo "Global litter mass" >> plot_features.txt # plot_title
echo "Litter mass (Pg)" >> plot_features.txt # y_label,  
**Conversion**: Units are **kgC/m2** (this is a pool) so by the grid area to get kgC/gridcell then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## CSOIL

file="cSoil_annually.nc"
echo "Global soil carbon mass" >> plot_features.txt # plot_title
echo "Soil carbon mass (Pg)" >> plot_features.txt # y_label, 
**Conversion**: Units are **kgC/m2** (this is a pool) so by the grid area to get kgC/gridcell then divide by 1E12 to get PgC. Sum across all cells to get an annual time series.

## LATENT HEAT 

file="hfls_annually.nc"
echo "Globally averaged latent heat flux over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Latent heat flux (W/m'^2*')'\"" >> plot_features.txt # 
**Conversion**: Units are **W/m2** so multiply by grid area then sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## SENSIBLE HEAT 

file="hfss_annually.nc"
echo "Globally averaged sensible heat flux over land (excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Sensible heat flux (W/m'^2*')'\"" >> plot_features.txt # y_label
**Conversion**: Units are **W/m2** so multiply by grid area then sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## NET DOWNWARD SHORTWAVE 

file="rss_annually.nc"
echo "Globally averaged net downward shortwave radiation over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Downward shortwave radiation (W/m'^2*')'\"" >> plot_features.txt # y_label, 
**Conversion**: Units are **W/m2** so multiply by grid area then sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## NET DOWNWARD LONGWAVE 

file="rls_annually.nc"
echo "Globally averaged net downward longwave radiation over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Downward longwave radiation (W/m'^2*')'\"" >> plot_features.txt # y_label
**Conversion**: Units are **W/m2** so multiply by grid area then sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## SHORTWAVE ALBEDO 

file="albs_annually.nc"
echo "Globally averaged shortwave broadband albedo over land (excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "Albedo (fraction)" >> plot_features.txt # y_label,  
**Conversion**: Units are **fraction** so multiply by grid area then sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## AREA BURNED

file="burntFractionAll_annually.nc"
echo "Global area burned " >> plot_features.txt # plot_title
echo "\"'Area burned (million km'^2*')'\"" >> plot_features.txt # y_label,
**Conversion**: Units are **fraction** so multiply by the grid area to get m2/grid then divide by 1E12 to get million km2. Sum across all cells to get an annual time series.

## EVAPORATION

file="evspsbl_annually.nc"
echo "Global evapotranspiration over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Evapotranspiration (1000 km'^3*'/yr)'\"" >> plot_features.txt # y_label, 
**Conversion**: Units are **kgH2O/m2/s** (this is a flux) so multiply by the grid area to get kg/gridcell/s multiply by the number of seconds in a year to get kg/gridcell/year which is equivalent to mm/gridcell/yr then divide by 1E15 to get 1000 km3/yr. Sum across all cells to get an annual time series.

## SOIL EVAPORATION

file="evspsblsoi_annually.nc"
echo "Global soil evaporation over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Soil Evaporation (1000 km'^3*'/yr)'\"" >> plot_features.txt # y_label, 
**Conversion**: Units are **kgH2O/m2/s** (this is a flux) so multiply by the grid area to get kg/gridcell/s multiply by the number of seconds in a year to get kg/gridcell/year which is equivalent to mm/gridcell/yr then divide by 1E15 to get 1000 km3/yr. Sum across all cells to get an annual time series.

## RUNOFF

file="mrro_annually.nc"
echo "Global runoff over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Runoff (1000 km'^3*'/yr)'\"" >> plot_features.txt # y_label,
**Conversion**: Units are **kgH2O/m2/s** (this is a flux) so multiply by the grid area to get kg/gridcell/s multiply by the number of seconds in a year to get kg/gridcell/year which is equivalent to mm/gridcell/yr then divide by 1E15 to get 1000 km3/yr. Sum across all cells to get an annual time series.

## PRECIPITATION

file="pr_annually.nc"
echo "Global precipitation over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Precipitation (1000 km'^3*'/yr)'\"" >> plot_features.txt # y_label,  
**Conversion**: Units are **kgH2O/m2/s** (this is a flux) so multiply by the grid area to get kg/gridcell/s multiply by the number of seconds in a year to get kg/gridcell/year which is equivalent to mm/gridcell/yr then divide by 1E15 to get 1000 km3/yr. Sum across all cells to get an annual time series.

## VEG HEIGHT

file="vegHeight_annually.nc"
echo "Globally averaged vegetation height over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "Vegetation height (m)" >> plot_features.txt # y_label, 
**Conversion**: Units are **m** so sum across all cells to get annual values then divide each value by the total grid area to get the global mean value.

## LITTER FALL 

in_file="**fVegLitter_monthly.nc**" - Note this uses the monthly file!
echo "Annual litterfall from vegetation to litter pool" >> plot_features.txt 
echo "Annual litterfall (Pg C/year)" >> plot_features.txt # y_label
**Conversion**: Units are **monthly** kgC/m2/s (this is a flux) so we multiply by the number of seconds in each month to get kgC/m2/month, sum the months in a year to get kgC/m2/yr then multiply by the grid area to get kgC/gridcell/yr then divide by 1E12 and sum across all cells to get an annual time series.

# Cumulative Annual Time Series

## Deforested biomass total cumulative

file="fDeforestTotal_annually.nc"
echo "Cumulative total deforested biomass" >> plot_features.txt # plot_title
echo "Cumulative deforested biomass (Pg C)" >> plot_features.txt # y_label, 
**Conversion**: Units are kgC/m2 (this is a pool) so we multiply by the grid area to get kgC/gridcell then divide by 1E12 to get PgC. Sum across all cells to get an annual time series. Then take a running total so the cumulative amount through time can be seen.

## NBP

file="nbp_annually.nc"
echo "Cumulative net biome productivity" >> plot_features.txt # plot_title
echo "Net biome productivity (Pg C)" >> plot_features.txt # y_label,
**Conversion**: Units are kgC/m2/s (this is a flux) so we multiply by the grid area to get kgC/gridcell/s multiply by the number of seconds in a year to get kgC/gridcell/year then divide by 1E12 to get PgC. Sum across all cells to get an annual time series. Then take a running total so the cumulative amount through time can be seen.

# Derived quantity time series

Generally will use two different files to derive something

## Carbon use efficiency

file="gpp_annually.nc"
file="npp_annually.nc"
We derive CUE = NPP/GPP. Since it is a ratio and since the units are the same, we don't need to do any conversions. We do need to multiply bu grid area and then normalize (divide by total grid area after summing) Watch out for places where GPP is 0 so you have a divide by 0 problem.

## T/ET or WUE

file="tran_annually.nc" (T)
file="evspsbl_annually.nc" (E)
We derive T/ET = T/(T + ET). Since it is a ratio and since the units are the same, we don't need to do any conversions. We do need to multiply bu grid area and then normalize (divide by total grid area after summing) Watch out for places where denom is 0 so you have a divide by 0 problem.


## SKIP: Annual maximum wetland extent

in_file="wetlandFrac_monthly.nc"
out_file="wetlandFrac_max_annually.nc"
cdo_cmd="-yearmax"

check_write_permission_and_add_file $in_file $out_file $cdo_cmd # this will loop over all run_names

file="wetlandFracMax_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Annual maximum wetland extent" >> plot_features.txt # plot_title
echo "\"'Annual maximum wetland extent (million km'^2*')'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
plot_annual_vals_time_series $file 2 # this does it for all run_names specified above

 ------------
 
# 5 panel monthly plots +  Obs [Optional] 

These generally have the same treatment as the timeseries plots. However these use monthly data rather than annual 

## Active layer depth

file="actlyrmax_monthly.nc"
label="Maximum monthly active layer depth (meters)"

## Wetland CH4 emissions
 
file="wetlandCH4dyn_monthly.nc"
label= "Wetland CH\$_{4}\$ emissions (Tg CH\$_{4}\$/m\$^{2}\$.month)

## Wetland extent

file="wetlandFrac_monthly.nc"
echo "Wetland extent (million km\$^{2}\$)" 

## Snow

file="snw_monthly.nc"
label="Snow amount (Kg/m\$^{2}\$)"

## Soil heterotrophic respiration

file="rhSoil_monthly.nc"
echo "Soil heretotrophic respiration (Pg C/month)" 
**Note the per month so you need to convert from /s and account for the differing number of seconds in each month due to different number of days.**

## Litter heterotrophic respiration

file="rhLitter_monthly.nc"
echo "Litter heretotrophic respiration (Pg C/month)" 

## Heterotrophic respiration

file="rh_monthly.nc"
echo "Heretotrophic respiration (Pg C/month)" 

## Autotrophic respiration

file="ra_monthly.nc"
echo "Autotrophic respiration (Pg C/month)" 

## Runoff

file="mrro_monthly.nc"
echo "Total Runoff" 
echo "Runoff (mm/month)" >> plot_features.txt # y_label

## LAI

file="lai_monthly.nc"
echo "Leaf area index" >> plot_features.txt # plot_title, sub and super scripts for python m^{2} echo "Leaf area index (m\$^{2}\$/m\$^{2}\$)" >> plot_features.txt # y_label

## Sensible heat

file="hfss_monthly.nc"
echo "Sensible heat flux" >> plot_features.txt # plot_title, 
echo "Sensible heat flux (W/m\$^{2}\$)" >> plot_features.txt # y_label

## Latent heat

file="hfls_monthly.nc"
echo "Latent heat flux" >> plot_features.txt # plot_title, 
echo "Latent heat flux (W/m\$^{2}\$)" >> plot_features.txt # y_label

## Litter fall

file="fVegLitter_monthly.nc"
echo "Litterfall" >> plot_features.txt # plot_title, 
echo "Litterfall flux from vegetation to litter pool (Pg C/month)" >> plot_features.txt # y_label

## Fire CO2 emissions

file="fFire_monthly.nc"
echo "Fire CO\$_{2}\$ emissions " >> plot_features.txt # plot_title, 
echo "Fire emissions (Pg C /month)" >> plot_features.txt # y_label

## ET from canopy
file="evspsblveg_monthly.nc"
#echo "Evapotranspiration from canopy" >> plot_features.txt # plot_title,
#echo "Evapotranspiration from canopy (mm/month)" >> plot_features.txt # y_label

## Soil evaporation

file="evspsblsoi_monthly.nc"
echo "Soil evaporation" >> plot_features.txt # plot_title, 
echo "Soil evaporation (mm/month)" >> plot_features.txt # y_label

## Area burned

file="burntFractionAll_monthly.nc"
echo "Global area burned " >> plot_features.txt # plot_title,
echo "Area burned (million km$^{2}$)" >> plot_features.txt # y_label

## Total ET

file="evspsbl_monthly.nc"
echo "Total evapotranspiration" >> plot_features.txt # plot_title, 
echo "Total evapotranspiration (mm/month)" >> plot_features.txt # y_label

## Vegetation biomass

file="cVeg_monthly.nc"
echo "Vegetation mass" >> plot_features.txt # plot_title, 
echo "Vegetation mass (Pg C)" >> plot_features.txt # y_label

## Soil carbon

file="cSoil_monthly.nc"
echo "Soil carbon mass" >> plot_features.txt # plot_title, 
echo "Soil carbon mass (Pg C)" >> plot_features.txt # y_label

## Litter carbon

file="cLitter_monthly.nc"
echo "Litter mass" >> plot_features.txt # plot_title
echo "Litter mass (Pg C)" >> plot_features.txt # y_label

## Albedo

file="albs_monthly.nc"
echo "Albedo" >> plot_features.txt # plot_title
echo "Albedo (fraction)" >> plot_features.txt # y_label

## Precipitation

file="pr_monthly.nc"
echo "Precipitation" >> plot_features.txt # plot_title
echo "Precipitation (mm/month)" >> plot_features.txt # y_label

## GPP

file="gpp_monthly.nc"
echo "Gross primary productivity" >> plot_features.txt # plot_title
echo "Gross primary productivity (PgC month$^{-1}$)" >> plot_features.txt # y_label

## NPP

file="npp_monthly.nc"
echo "Net primary productivity" >> plot_features.txt # plot_title
echo "Net primary productivity (PgC month$^{-1}$)" >> plot_features.txt # y_label

## NBP

file="nbp_monthly.nc"
echo "Net biome productivity" >> plot_features.txt # plot_title
echo "Net biome productivity (PgC month$^{-1}$)" >> plot_features.txt # y_label

## Net longwave

file="rls_monthly.nc"
echo "Net longwave radiation (+ve down)" >> plot_features.txt # plot_title
echo "Net longwave radiation (W/m\$^{2}\$)" >> plot_features.txt # y_label

## Net shortwave

file="rss_monthly.nc"
echo "Net shortwave radiation (+ve down)" >> plot_features.txt # plot_title
echo "Net shortwave radiation (W/m\$^{2}\$)" >> plot_features.txt # y_label

# Stop here
------------------

# 2D geographical anomaly plots against observations.

## GPP

file="gpp_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/GPP/Beer_2010/beer_t63_final.nc"
echo "Gross primary productivity" >> plot_features.txt # plot_title
echo "50, 100, 200, 300, 500, 1000, 1500, 2000, 2500, 3000" >> plot_features.txt # color bar levels (maximum 10)
echo "\$g\ C\ m^{-2}\ year^{-1} $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
echo "Gross primary productivity anomalies compared to Beer et al. (2010)" >> plot_features.txt # [OPTIONAL] plot_title for anomalies plot
echo "-500, -400, -300, -200, -100, 100, 200, 300, 400, 500" >> plot_features.txt              # [OPTIONAL] color bar levels (maximum 10) for anomalies from observations
echo "Mustard_White_DarkBlue" >> plot_features.txt                                              # [OPTIONAL] color scheme for anomalies
**Conversion:** Kg C/m2.s  x 31536000 = kg C/m2.year x 1000 = g C/m2.year

## Fraction burned

file="burntFractionAll_annually.nc"
obs_file1="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Fire/GFEDv4/GFED4s_percent_burnedarea_t63_mean_annual_1997_2014.nc"
cdo -divc,100 $obs_file1 obs_file_temp.nc # observed burned area is percentage
obs_file="obs_file_temp.nc"
echo "Area burned" >> plot_features.txt # plot_title
echo "0.001, 0.005, 0.01, 0.05, 0.1, 0.15, 0.20, 0.30, 0.40, 0.50" >> plot_features.txt # color bar levels (maximum 10)
echo "Fraction of grid cell" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
#plot_2D_geographical_plot_last_N_years $file
echo "Area burned anomalies compared to GFED4s" >> plot_features.txt # [OPTIONAL] plot_title for anomalies plot
echo "-0.3, -0.2, -0.1, -0.05, -0.01, 0.01, 0.05, 0.1, 0.2, 0.3" >> plot_features.txt              # [OPTIONAL] color bar levels (maximum 10) for anomalies from observations
echo "Mustard_White_DarkBlue" >> plot_features.txt                                              # [OPTIONAL] color scheme for anomalies

## Annual maximum wetland fraction

in_file="wetlandFrac_monthly.nc"
out_file="wetlandFrac_max_annually.nc"
cdo_cmd="-yearmax"

check_write_permission_and_add_file $in_file $out_file $cdo_cmd # this will loop over all run_names

file="wetlandFrac_max_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Wetlands/GCP-SWAMPS+GLWD/GCP-SWAMPS+GLWD_t63_res_ymonmean.nc"
cdo -yearmax $obs_file obs_file_temp.nc # obs annual maximum wetland fraction
obs_file="obs_file_temp.nc"

rm -f plot_features.txt; touch -f plot_features.txt
echo "Annual maximum wetland fraction" >> plot_features.txt # plot_title
echo "0.001, 0.005, 0.01, 0.1, 0.2, 0.3, 0.40, 0.50, 0.70, 0.90" >> plot_features.txt # color bar levels (maximum 10)
echo "Fraction of grid cell" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
#plot_2D_geographical_plot_last_N_years $file
echo "Wetland fraction anomalies compared to SWAMPS+GLWD" >> plot_features.txt # [OPTIONAL] plot_title for anomalies plot
echo "-0.3, -0.2, -0.1, -0.05, -0.01, 0.01, 0.05, 0.1, 0.2, 0.3" >> plot_features.txt              # [OPTIONAL] color bar levels (maximum 10) for anomalies from observations
echo "Mustard_White_DarkBlue" >> plot_features.txt                                              # [OPTIONAL] color scheme for anomalies
plot_2D_geographical_plot_last_N_years $file 1 $obs_file

## Wetland emissions

file="wetlandCH4dyn_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "\$Wetland \ CH_{4}\ emissions\$ " >> plot_features.txt # plot_title
echo "0.01, 0.05, 0.1, 1.0, 2.0, 3.0, 4.0, 5.0, 7.0, 9.0" >> plot_features.txt # color bar levels (maximum 10)
echo "\$g\ CH_{4}\ m^{-2}\ year^{-1}  $" >> plot_features.txt # color bar title
echo "White_LightBlue_Green_Yellow_Orange_Red_Brown" >> plot_features.txt # color scheme
plot_2D_geographical_plot_last_N_years $file 31536000000 

# Zonal avgs

## GPP

file="gpp_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/GPP/Beer_2010/beer_t63_final.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Gross primary productivity" >> plot_features.txt # plot_title
echo "\"'Gross primary productivity (g C/m'^2*'.yr)'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "Beer et al. (2010)" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise

convert Kg C/m2.s -> g C/m2.yr. x 31536000 x 1000
plot_zonally_averaged_annual_vals $file 31536000000 $obs_file # 1 is the multiplier for $file, $obs_file is optional


## LAI 

file="lai_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/LAI/BU-MODIS_LAI_MONTHLY_T63_3_annual_max.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Annual maximum LAI over land@(excluding Greenland and Antarctica) " >> plot_features.txt # plot_title
echo "\"'Leaf area index (m'^2*'/m'^2*')'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "BU MODIS" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
plot_zonally_averaged_annual_vals $file 1 $obs_file # 1 is the multiplier for $file, $obs_file is optional


## Vegetation biomass
file="cVeg_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Vegetation_biomass/Poulter/Reusch_Gibbs_2008_veg_biomass_t63.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Vegetation biomass" >> plot_features.txt # plot_title
echo "\"'Vegetation biomass (kg C/m'^2*')'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "Reusch and Gibbs (2008)" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
plot_zonally_averaged_annual_vals $file 1 $obs_file # 1 is the multiplier for $file, $obs_file is optional


## Total ET

file="evspsbl_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Total evapotranspiration" >> plot_features.txt # plot_title
echo "Evapotranspiration (mm/year)" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
convert Kg/m2.s -> Kg/m2.yr. x 31536000 
plot_zonally_averaged_annual_vals $file 31536000 # 1 is the multiplier for $file, 3rd argument $obs_file is optional


## Soil carbon

file="cSoil_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Soils/HWSD/OUTPUT/global_soilC_stocks_t63.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Soil carbon mass" >> plot_features.txt # plot_title
echo "\"'Soil carbon mass (kg C/m'^2*')'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "Harmonized world soil database v1.2" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
plot_zonally_averaged_annual_vals $file 1 $obs_file # 1 is the multiplier for $file, 3rd argument $obs_file is optional


## Fraction burnt 

file="burntFractionAll_annually.nc"
obs_file1="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Fire/GFEDv4/GFED4s_percent_burnedarea_t63_mean_annual_1997_2014.nc"
cdo -divc,100 $obs_file1 obs_file_temp.nc # observed burned area is percentage
obs_file="obs_file_temp.nc"

rm -f plot_features.txt; touch -f plot_features.txt
echo "Area burned " >> plot_features.txt # plot_title
echo "Fraction burned" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "GFED 4s" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
plot_zonally_averaged_annual_vals $file 1 $obs_file # 1 is the multiplier for $file, 3rd argument $obs_file is optional


## fire CO2 emissions

file="fFire_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Fire/GFEDv4/GFED4s_annual_fire_emissions_t63_1997_2014_gCO2_per_year.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "\"'Fire CO'[2]*' emissions'\"" >> plot_features.txt # plot_title
echo "\"'Fire CO'[2]*' emissions (gCO'[2]*'/m'^2*'.yr)'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "GFED 4s" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
convert Kg CO2/m2.s -> g CO2/m2.yr. x 31536000 x 1000 
plot_zonally_averaged_annual_vals $file 31536000000 $obs_file # 1 is the multiplier for $file, 3rd argument $obs_file is optional

## Litter carbon

file="cLitter_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Litter mass" >> plot_features.txt # plot_title
echo "\"'Litter mass (kg C/m'^2*')'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
plot_zonally_averaged_annual_vals $file 1  # 1 is the multiplier for $file, 3rd argument $obs_file is optional

## Total land carbon

file="cLand_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Total carbon on land" >> plot_features.txt # plot_title
echo "\"'Land carbon mass (kg C/m'^2*')'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
plot_zonally_averaged_annual_vals $file 1  # 1 is the multiplier for $file, 3rd argument $obs_file is optional

## Wetland emissions
file="wetlandCH4dyn_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "\"'Wetland CH'[4]*' emissions'\"" >> plot_features.txt # plot_title
echo "\"'Wetland CH'[4]*' emissions (gCH'[4]*'/m'^2*'.yr)'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
convert Kg CH4/m2.s -> g CH4/m2.yr. x 31536000 x 1000
plot_zonally_averaged_annual_vals $file 31536000000 # 1 is the multiplier for $file, 3rd argument $obs_file is optional



## Annual maximum wetland fraction 

 This is done slightly differently using the monthly file and the script then finds
annual monthly maximum across all years, and then zonally averages them. Note also two observation based data sets are passed in. Use this as an example when passing in two sets of observations when plotting zonally-averaged values

in_file="wetlandFrac_monthly.nc"
out_file="wetlandFrac_max_annually.nc"
cdo_cmd="-yearmax"

check_write_permission_and_add_file $in_file $out_file $cdo_cmd # this will loop over all run_names

file="wetlandFrac_max_annually.nc"
obs_file="/raid/rd40/data/CTEM/OBSERVATION_DATASETS/Wetlands/GLWD/GLWD_T63_res.nc /raid/rd40/data/CTEM/OBSERVATION_DATASETS/Wetlands/GCP-SWAMPS+GLWD/GCP-SWAMPS+GLWD_t63_res_ymonmean.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Zonally-averaged annual maximum wetland fraction" >> plot_features.txt # plot_title
echo "Wetland fraction" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
echo "GLWD" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
echo "SWAMPS + GLWD merged dataset" >> plot_features.txt # legend name for observations, not used if $obs_file is not specified but needed otherwise
plot_zonally_averaged_annual_vals $file 1  "$obs_file" # 1 is the multiplier for $file, 3rd argument $obs_file is optional


## Litter fall

in_file="fVegLitter_monthly.nc"
out_file="fVegLitter_annually.nc"
cdo_cmd="-yearsum"

check_write_permission_and_add_file $in_file $out_file $cdo_cmd # this will loop over all run_names

file="fVegLitter_annually.nc"
rm -f plot_features.txt; touch -f plot_features.txt
echo "Annual litter fall" >> plot_features.txt # plot_title
echo "\"'Litter fall rate  (gC/m'^2*'.yr)'\"" >> plot_features.txt # y_label, use "Label with no sub/superscripts" or "\"'Latent heat W/m'^2\"" or "\"'Latent heat W/m'[2]\"" 
plot_zonally_averaged_annual_vals $file 1  # 1 is the multiplier for $file, 3rd argument $obs_file is optional



# MONTHLY PLOTS   FOR 3D VARIABLES LIKE SOIL MOISTURE AND TEMPERATURE

## Liquid soil moisture

file="mrsll_monthly.nc"
label ="Liquid soil moisture (m\$^{3}\$/m\$^{3}\$)" 
 
## Frozen soil moisture

file="mrsfl_monthly.nc"
label = "Frozen soil moisture" (m\$^{3}\$/m\$^{3}\$)"

## soil temperature

file="tsl_monthly.nc"
label= "Soil temperature (\$^\circ\$ C)"



> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjM1NDI4NzMyLDE3OTMyNTEzMDAsMTczMD
M0ODA0OCwxOTA4NTEzNDI5LDExOTk2ODM0OTksMjkwMzAzMzQw
LC0xMTk3ODU1NzkyLDcyMTg2ODY1NywxOTM5ODc3ODM1LDE3MD
YzOTYyMDcsLTQ3NjMwMTE5MiwyMDU2NTMwNDE2LC0xMTMzNDIy
NTUyLC0xMzI1NDg1MDExLDIwMTEwNjYyNzFdfQ==
-->