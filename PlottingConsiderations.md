
# Main considerations when designing plotting framework


## Workflow
 - Run model
 - {Trigger AMBER} (option)
 - Trigger plotting framework
 - Serve outputs to https://goc-dx.science.gc.ca
 - Email user once complete

## General structure considerations
- Modular
- Written in Python (? Julia suggested?)
- Easily expanded
- Can incorporate legacy analysis (e.g. boreholes analysis from my permafrost paper)
- Uses switches to turn on/off functionality
	- Some analysis can be slow so may not want done every time
- Jupyter notebooks?
- Can integrate observations
	- To investigate model vs. 'reality'
- Can integrate multiple model runs into plots
	- To compare model development through time
- Developed and deployed within conda environment
- **Very** well documented
	- This framework will be adapted by users other than the developer.
- Ideally incorporate dynamic plotting capabilities but also needs to produce static plots suitable for publication
- Leave major statistical analysis and benchmarking vs. observations to AMBER
- Parallelization desirable as we will have HPC at disposal

## CLASSIC output considerations 
- All outputs are netcdf format
- Three main model configurations:
	- Global (regular lon-lat grid)
	- Regional (projected/irregular lon-lat grid)
	- Site-level
- Each variable can be output at a different time frequency:
	- Annually
	- Monthly
	- Daily
	- Half-hourly
- Some variables can have different dimensions
	- E.g. GPP can be 
		- per grid cell (lon,lat,time)
		- per PFT (vegetation type) (lon,lat,time,pft)
		- per tile (sub area of grid cell) (lon,lat,time,tile)
	- Others like soil carbon can also be per soil layer in addition to the above example. 

> Written with [StackEdit](https://stackedit.io/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTMyNTI0NTQyNywtMzA1MDA0NDQ4LDY3OT
A3NzY4NF19
-->